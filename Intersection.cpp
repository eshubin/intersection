/* 
 * File:   Intersection.cpp
 * Author: evgeniy
 * 
 * Created on August 13, 2012, 11:43 PM
 */

#include "Intersection.h"

#include <algorithm>

#include <boost/foreach.hpp>

void ChooseShortest(const SegmentList& lhs, const SegmentList& rhs,
                    const SegmentList** shortest, const SegmentList ** longest)
{
    if (lhs.size() < rhs.size())
    {
        *shortest = &lhs;
        *longest = &rhs;
    }
    else
    {
        *shortest = &rhs;
        *longest = &lhs;
    }
}

SegmentList Intersect(const SegmentList& lhs, const SegmentList& rhs)
{
    const SegmentList *shortest = NULL, *longest = NULL;
    ChooseShortest(lhs, rhs, &shortest, &longest);

//    const SegmentList *shortest = &lhs, *longest = &rhs;

    SegmentList res;

    SegmentList::const_iterator current = longest->begin();

    BOOST_FOREACH(const Segment &seg, *shortest)
    {
        current = std::lower_bound(
                                   current,
                                   longest->end(),
                                   std::make_pair(0, seg.first),
                                   LessFinish
                                   );

        if ((current != longest->end()) && (seg.first >= current->first) && (seg.first <= current->second))
        {
            res.push_back(std::make_pair(
                                         std::max(seg.first, current->first),
                                         std::min(seg.second, current->second)
                                         ));
            ++current;
        }

        current = std::lower_bound(
                                   current,
                                   longest->end(),
                                   seg, LessStart
                                   );

        for (; ((current != longest->end()) && (seg.second >= current->first)); ++current)
        {
            res.push_back(std::make_pair(
                                         std::max(seg.first, current->first),
                                         std::min(seg.second, current->second)
                                         ));
        }
        --current;
    }

    return res;
}

bool LessStart(const Segment &lhs, const Segment &rhs)
{
    return lhs.first < rhs.first;
}

bool LessFinish(const Segment &lhs, const Segment &rhs)
{
    return lhs.second < rhs.second;
}

std::ostream& operator<<(std::ostream& out, const SegmentList& list)
{
    bool first = true;
    out << '[';

    BOOST_FOREACH(const Segment& s, list)
    {
        if (first)
        {
            first = false;
        }
        else
        {
            out << ", ";
        }
        out << '{' << s.first << ',' << s.second << '}';
    }
    out << ']';
    return out;
}

