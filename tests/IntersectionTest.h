/*
 * File:   IntersectionTest.h
 * Author: evgeniy
 *
 * Created on Aug 13, 2012, 11:40:37 PM
 */

#ifndef INTERSECTIONTEST_H
#define	INTERSECTIONTEST_H

#include <cppunit/extensions/HelperMacros.h>

class IntersectionTest : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE(IntersectionTest);

    CPPUNIT_TEST(testEmpty);
    CPPUNIT_TEST(testLess);
    CPPUNIT_TEST(testSingle);
    CPPUNIT_TEST(testChooseShortest);
    CPPUNIT_TEST(testShifted);
    CPPUNIT_TEST(testShifted2);

    CPPUNIT_TEST_SUITE_END();

public:
    IntersectionTest();
    virtual ~IntersectionTest();
    //    void setUp();
    //    void tearDown();

private:
    void testEmpty();
    void testSingle();
    void testChooseShortest();
    void testLess();
    void testShifted();
    void testShifted2();
};

#endif	/* INTERSECTIONTEST_H */

