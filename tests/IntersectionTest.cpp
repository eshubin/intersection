/*
 * File:   IntersectionTest.cpp
 * Author: evgeniy
 *
 * Created on Aug 13, 2012, 11:40:38 PM
 */

#include "IntersectionTest.h"

#include "Intersection.h"


CPPUNIT_TEST_SUITE_REGISTRATION(IntersectionTest);

IntersectionTest::IntersectionTest()
{
}

IntersectionTest::~IntersectionTest()
{
}

//void IntersectionTest::setUp() {
//}
//
//void IntersectionTest::tearDown() {
//}


void IntersectionTest::testEmpty()
{
    SegmentList res, lhs, rhs;
    CPPUNIT_ASSERT(res == Intersect(lhs, rhs));
}

void IntersectionTest::testSingle()
{
    SegmentList res, lhs, rhs;
    
    lhs.push_back(std::make_pair(1,2));
    rhs.push_back(std::make_pair(1,2));
    res.push_back(std::make_pair(1,2));
    
    CPPUNIT_ASSERT(res == Intersect(lhs, rhs));
}

void IntersectionTest::testShifted()
{
    SegmentList res, lhs, rhs;
    
    rhs.push_back(std::make_pair(0,2));
    lhs.push_back(std::make_pair(1,4));
    res.push_back(std::make_pair(1,2));
    
    CPPUNIT_ASSERT_EQUAL(res, Intersect(lhs, rhs));
    CPPUNIT_ASSERT_EQUAL(res, Intersect(rhs, lhs));
}

void IntersectionTest::testShifted2()
{
    SegmentList res, lhs, rhs;
    
    lhs.push_back(std::make_pair(0,12));
    lhs.push_back(std::make_pair(13,15));
    
    rhs.push_back(std::make_pair(1,4));
    rhs.push_back(std::make_pair(5,6));
    rhs.push_back(std::make_pair(8,8));
    rhs.push_back(std::make_pair(11,14));

    
    res.push_back(std::make_pair(1,4));
    res.push_back(std::make_pair(5,6));
    res.push_back(std::make_pair(8,8));
    res.push_back(std::make_pair(11,12));
    res.push_back(std::make_pair(13,14));

    CPPUNIT_ASSERT_EQUAL(res, Intersect(lhs, rhs));
    CPPUNIT_ASSERT_EQUAL(res, Intersect(rhs, lhs));
}



void IntersectionTest::testChooseShortest()
{
    SegmentList lhs, rhs;
    const SegmentList *shortest = NULL, *longest = NULL;
    lhs.push_back(std::make_pair(1,2));
    ChooseShortest(lhs, rhs, &shortest, &longest);
    CPPUNIT_ASSERT_EQUAL(const_cast<const SegmentList*>(&lhs), longest);
    CPPUNIT_ASSERT_EQUAL(const_cast<const SegmentList*>(&rhs), shortest);
}

void IntersectionTest::testLess()
{
    Segment lhs = std::make_pair(1, 3),
            rhs = std::make_pair(2, 8);
    CPPUNIT_ASSERT(LessStart(lhs, rhs));
    CPPUNIT_ASSERT(LessFinish(lhs, rhs));
}


