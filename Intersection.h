/* 
 * File:   Intersection.h
 * Author: evgeniy
 *
 * Created on August 13, 2012, 11:43 PM
 */

#ifndef INTERSECTION_H
#define	INTERSECTION_H

#include <vector>
#include <iostream>

typedef std::pair<int, int> Segment;
typedef std::vector<Segment> SegmentList;


std::ostream& operator<<(std::ostream& out, const SegmentList& list);

SegmentList Intersect(const SegmentList& lhs, const SegmentList& rhs);
void ChooseShortest(const SegmentList& lhs, const SegmentList& rhs,
                    const SegmentList** shortest, const SegmentList ** longest);

bool LessStart(const Segment &lhs, const Segment &rhs);
bool LessFinish(const Segment &lhs, const Segment &rhs);

#endif	/* INTERSECTION_H */

